# Exercise Solutions - Learning Python
Learning Python, 5 Edition is a book written by Mark Lutz.

# Chapter 1 
This document holds notes and solutions for the exercises in chapter 1 of the
book.

# Exercise 1 - Interaction
The Python environment on a machine running Linux Mint 18.3 Cinnamon 64-bit was
tested in this exercise.

A terminal window was started by the use of the Linux graphical interface.

The first command used in the terminal was:

```
$ python3
```

This started the Python interactive command line with the chevron prompt. The 
following details where printed at the startup:

```
$ python3
Python 3.5.2 (default, Sep 14 2017, 22:51:06) 
[GCC 5.4.0 20160609] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

The expression "Hello World" was then entered at the chevron prompt:

```
$ python3
Python 3.5.2 (default, Sep 14 2017, 22:51:06) 
[GCC 5.4.0 20160609] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> "Hello World"
```

The result was that the entered string was echoed back by the Python 
interpreter:

```
$ python3
Python 3.5.2 (default, Sep 14 2017, 22:51:06) 
[GCC 5.4.0 20160609] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> "Hello World"
'Hello World'
```

Everything worked as expected and the Python environment seemed to be setup
correctly and everything is ready for more experiments to learn Python.
